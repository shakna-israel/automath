#!/usr/bin/wolfram -script

input = ToExpression[Input["Type values, seperated by commas: "]]
array = \[RawLeftBrace] <> input <> \[RawRightBrace]
Print[array];
