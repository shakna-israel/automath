#Automath

This is a simple Wolfram script to fetch some standard equations on data the user inputs.

Running is as simple as:
```
./main
```

However, you may face some issues, these solutions may help:

## Permissions

You may have to allow main to execute as a program.
```
chmod +x main
```

## Locations

Over the years, the location and the way you run the scripting engine for Mathematica has changed greatly. Automath was made with Mathematica 10 in mind.

You may have to edit the first line in main to reflect your own version:

```
#!/usr/bin/env MathKernel
```

or

```
#!/usr/local/bin/math
```

or

```
#!/usr/local/bin/MathematicaScript -script
```

or

```
#!/usr/loca/bin/WolframScript -script
```

#Expressions

Automath aims to produce outputs from an array you feed in, through these expressions:

* Mean
* Median
* Mode
* Standard Deviation
* Standard Error
* Z-Scores
* Confidence Interval
* Normal Distribution
* Statistical Power
* Frequency Distribution
